import * as types from '../../consts/actionTypes';

export const getWeatherRequest = city => ({
	type: types.GET_WEATHER_REQUEST,
	city
});
