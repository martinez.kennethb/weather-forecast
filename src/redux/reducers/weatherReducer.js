import * as types from '../../consts/actionTypes';

export default function(state = [], action) {
	switch (action.type) {
		case types.GET_WEATHER_REQUEST:
			return [...state];
		case types.GET_WEATHER_SUCCESS:
			if (action.weather) {
				return [...action, action.weather];
			}
			return [...state, action];
		default:
			return state;
	}
}
