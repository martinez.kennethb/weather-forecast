import { takeLatest } from 'redux-saga/effects';
import { getWeatherRequestSaga } from './weatherSaga';

import * as types from '../../consts/actionTypes';

export default function* watchDemo() {
	yield takeLatest(types.GET_WEATHER_REQUEST, getWeatherRequestSaga);
}
