import { put, call } from 'redux-saga/effects';
import { getWeather } from '../api';
import * as types from '../../consts/actionTypes';

export function* getWeatherRequestSaga({ city }) {
	try {
		const weather = yield call(getWeather, city);

		yield put({ type: types.GET_WEATHER_SUCCESS, weather });
	} catch (error) {
		yield put({ type: types.GET_WEATHER_ERROR, error });
	}
}
