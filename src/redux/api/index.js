import { API_KEY } from '../../consts/actionTypes'
const axios = require('axios');

export const getWeather = city => {
	const URL = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&cnt=5&appid=${API_KEY}`

	return axios.get(URL).then(response => {
		return response.data;
	});
};
