import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { getWeatherRequest } from '../../redux/actions/weatherActions'
import Forecast from '../../components/Forecast'
import Form from '../../components/Form'
import { Container, Header, WeatherContainer } from '../../styles'

class Home extends Component {
	constructor(props) {
    super(props)
      this.state = {
        city: '',
    }
  }
  onChange = (e) => {
    this.setState({ city: e.target.value })
  }

	search = (e) => {
		const { city } = this.state
		const { getWeather } = this.props
		e.preventDefault()
		getWeather(city)

	}
	render() {
		const { city } = this.state
		const { weather } = this.props
		return (
			<Container>
				<Header>
					Weather Forecast
				</Header>
				<WeatherContainer>
					<Form
						onChange={this.onChange}
						onSearch={this.search}
						value={city}
					/>
					{weather &&
					<Forecast weather={weather} />
					}
				</WeatherContainer>
			</Container>
		)
}
}

const mapDispatchToProps = (dispatch, props) => {
	return {
		getWeather: city => {
			dispatch(getWeatherRequest(city))
		}
	}
}
const mapStateToProps = state => {
	return {
		weather: state.weatherReducer[0],
	}
}

Home.propTypes = {
	dispatch: PropTypes.func,
	weather: PropTypes.object,
	getWeather: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
