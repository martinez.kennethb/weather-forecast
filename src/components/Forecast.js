import React from "react"
import moment from 'moment'
import { ForecastContainer, Title, Weather, Item, Day, Minmax, Max, Min } from '../styles'

const Forecast = ({ weather }) => (
  <ForecastContainer>
    <Title>{`This is the weather in ${weather.city.name} for the next 5 days`}</Title>
    <Weather>
      {
        weather.list.map((value, index) => (
          <Item key={value.dt}>
            <Day>{moment().startOf(moment().day()).add(index, 'day').format('dddd')}</Day>
            <img src={`http://openweathermap.org/img/w/${value.weather[0].icon}.png`} />
            <Minmax>
              <Max>
                {`${Math.trunc(value.main.temp_max)}ºC`}
              </Max>
              <Min>
                {`${Math.trunc(value.main.temp_min)}ºC`}
              </Min>
            </Minmax>
          </Item>
        ))
      }
    </Weather>
  </ForecastContainer>
)

export default Forecast;
