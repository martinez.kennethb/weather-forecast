import React from "react"
import { FormContainer, Input, Button } from '../styles'

const Form = ({ onChange, onSearch, city }) => (
  <FormContainer>
    <label>Please enter the city you want to search</label>
    <Input name="city" onChange={onChange} value={city} />
    <Button onClick={onSearch}>SEARCH</Button>
  </FormContainer>
)

export default Form;
