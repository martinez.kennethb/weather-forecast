import styled from 'styled-components'

export const Container = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100%;
	height: 100vh;
	flex-direction: column;
	font-family: arial;
`
export const Header = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	padding: 24px;
	box-sizing: border-box;
	background-color: #008CE0;
	width: 100%;
	height: 20vh;
	font-size: 24px;
	color: #FFFFFF;
`

export const WeatherContainer = styled.div`
	display: flex;
	width: 100%;
	height: 80vh;
	flex-direction: column;
`

 export const FormContainer = styled.form`
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 24px;
	flex-direction: column;
	box-sizing: border-box;
	width: 100%;
	height: 200px;
`

export const Input = styled.input`
	width: 300px;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
`

export const Button = styled.button`
	width: 300px;
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	border-radius: 4px;
	cursor: pointer;
	font-size: 14px;
`

export const ForecastContainer = styled.div`
  width: 100%;
  height: 400px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
`

export const Weather = styled.div`
  width: 80%;
  margin-top: 20px;
  height: auto;
  display: flex;
  justify-content: space-around;
  align-items: center;
`

export const Item = styled.div`
  width: 100px;
  height: 110px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
`

export const Minmax = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
`

export const Title = styled.span`
  font-size: 20px;
  color: #008CE0;
`

export const Min = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
  color: #9e9e9e;
`

export const Max = styled(Min)`
  color: #353535;
`

export const Day = styled.span`
`
